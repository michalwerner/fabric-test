import { fabric } from "fabric";
import { useState, useEffect, useRef, useCallback } from "react";

import createFabricCanvas from "./../studio/canvas";
import createFabricObject from "./../studio/object";

const getInitialObjects = () => [
  createFabricObject({
    left: 100,
    top: 100,
  }),
  createFabricObject({
    width: 50,
    height: 50,
    left: 300,
    top: 100,
    fill: "red",
    borderColor: "red",
    cornerColor: "red",
    cornerStrokeColor: "blue",
  }),
  createFabricObject({
    left: 200,
    top: 300,
    fill: "green",
    borderColor: "green",
    cornerColor: "green",
    cornerStrokeColor: "green",
  }),
  createFabricObject({
    width: 170,
    height: 50,
    left: 100,
    top: 600,
    fill: "purple",
  }),
  createFabricObject({
    width: 100,
    height: 500,
    left: 600,
    top: 250,
    fill: "black",
  }),
];

const useCanvas = () => {
  const ref = useRef();
  const [canvas] = useState(createFabricCanvas());
  const [activeCustomObjectData, setActiveCustomObjectData] = useState(null);

  const updateActiveCustomObject = useCallback(
    (target) => {
      const activeObj = target || canvas.getActiveObject();
      setActiveCustomObjectData(
        activeObj instanceof fabric.CustomObject
          ? { target: activeObj, left: activeObj.left, top: activeObj.top }
          : null
      );
    },
    [canvas]
  );

  useEffect(() => {
    canvas.initialize(ref.current, {
      width: window.innerWidth,
      height: window.innerHeight,
    });

    canvas.on("selection:created", (options) => {
      updateActiveCustomObject();
    });

    canvas.on("selection:updated", (options) => {
      updateActiveCustomObject();
    });

    canvas.on("selection:cleared", (options) => {
      updateActiveCustomObject();
    });

    canvas.on("object:moving", (options) => {
      updateActiveCustomObject(options.target);
    });

    canvas.on("object:scaled", (options) => {
      updateActiveCustomObject(options.target);
    });

    canvas.on("object:rotated", (options) => {
      updateActiveCustomObject(options.target);
    });

    canvas.on("object:skewed", (options) => {
      updateActiveCustomObject(options.target);
    });

    canvas.add(...getInitialObjects());

    canvas.renderAll();
  }, [canvas, updateActiveCustomObject]);

  const setRef = useCallback((node) => {
    ref.current = node;
  }, []);

  return [setRef, activeCustomObjectData];
};

export default useCanvas;
