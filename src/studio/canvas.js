import { fabric } from "fabric";

(function () {
  fabric.CustomCanvas = fabric.util.createClass(fabric.Canvas, {
    initialize: function (el, options) {
      this.callSuper("initialize", el, options);
    },
  });
})();

const createFabricCanvas = () => {
  const instance = new fabric.CustomCanvas("canvas");

  return instance;
};

export default createFabricCanvas;
