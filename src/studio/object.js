import { fabric } from "fabric";

import { renderTriangleControl } from "./triangleControl";

(function () {
  const { controlsUtils } = fabric;

  const cornerSize = 20;
  const cornerOffset = cornerSize / 2 + 10;

  const adjustCornerOffset = (type) =>
    cornerSize / (type === "triange" ? 2 : 1);

  const controlParamsFromPosition = (position, type) => {
    switch (position) {
      case "tr":
        return {
          x: 0.5,
          y: -0.5,
          offsetX: cornerOffset,
          offsetY: -cornerOffset,
          actionHandler: controlsUtils.scalingEqually,
          cursorStyleHandler: controlsUtils.scaleCursorStyleHandler,
        };
      case "mr":
        return {
          x: 0.5,
          y: 0,
          offsetX: adjustCornerOffset(type),
          offsetY: 0,
          actionHandler: controlsUtils.scalingXOrSkewingY,
          cursorStyleHandler: controlsUtils.scaleSkewCursorStyleHandler,
        };
      case "br":
        return {
          x: 0.5,
          y: 0.5,
          offsetX: cornerOffset,
          offsetY: cornerOffset,
          actionHandler: controlsUtils.scalingEqually,
          cursorStyleHandler: controlsUtils.scaleCursorStyleHandler,
        };
      case "mb":
        return {
          x: 0,
          y: 0.5,
          offsetX: 0,
          offsetY: adjustCornerOffset(type),
          actionHandler: controlsUtils.scalingYOrSkewingX,
          cursorStyleHandler: controlsUtils.scaleSkewCursorStyleHandler,
        };
      case "bl":
        return {
          x: -0.5,
          y: 0.5,
          offsetX: -cornerOffset,
          offsetY: cornerOffset,
          actionHandler: controlsUtils.scalingEqually,
          cursorStyleHandler: controlsUtils.scaleCursorStyleHandler,
        };
      case "ml":
        return {
          x: -0.5,
          y: 0,
          offsetX: -adjustCornerOffset(type),
          offsetY: 0,
          actionHandler: controlsUtils.scalingXOrSkewingY,
          cursorStyleHandler: controlsUtils.scaleSkewCursorStyleHandler,
        };
      case "tl":
        return {
          x: -0.5,
          y: -0.5,
          offsetX: -cornerOffset,
          offsetY: -cornerOffset,
          actionHandler: controlsUtils.scalingEqually,
          cursorStyleHandler: controlsUtils.scaleCursorStyleHandler,
        };
      case "mt":
        return {
          x: 0,
          y: -0.5,
          offsetX: 0,
          offsetY: -adjustCornerOffset(type),
          actionHandler: controlsUtils.scalingYOrSkewingX,
          cursorStyleHandler: controlsUtils.scaleSkewCursorStyleHandler,
        };
      default:
        return {};
    }
  };

  const getResizeControl = (position) =>
    new fabric.Control({
      ...controlParamsFromPosition(position, "triange"),
      render: function (ctx, left, top, styleOverride, fabricObject) {
        renderTriangleControl.call(
          this,
          ctx,
          left,
          top,
          styleOverride,
          fabricObject,
          position
        );
      },
    });

  const getRotateControl = (position) =>
    new fabric.Control({
      ...controlParamsFromPosition(position),
      actionHandler: controlsUtils.rotationWithSnapping,
      cursorStyleHandler: controlsUtils.rotationStyleHandler,
      render: function (ctx, left, top, styleOverride, fabricObject) {
        fabric.controlsUtils.renderCircleControl.call(
          this,
          ctx,
          left,
          top,
          styleOverride,
          fabricObject
        );
      },
    });

  fabric.CustomObject = fabric.util.createClass(fabric.Rect, {
    initialize: function (options) {
      options = {
        width: 200,
        height: 200,
        fill: "yellow",
        padding: 10,
        borderColor: "black",
        cornerColor: "black",
        cornerStrokeColor: "black",
        cornerSize,
        ...options,
      };

      this.callSuper("initialize", options);
    },

    controls: {
      tr: getResizeControl("tr"),
      bl: getResizeControl("bl"),

      tl: getRotateControl("tl"),
      br: getRotateControl("br"),

      // uncomment code below to test other combinations

      // tr: getResizeControl("tr"),
      // mr: getResizeControl("mr"),
      // br: getResizeControl("br"),
      // mb: getResizeControl("mb"),
      // bl: getResizeControl("bl"),
      // ml: getResizeControl("ml"),
      // tl: getResizeControl("tl"),
      // mt: getResizeControl("mt"),

      // tr: getRotateControl("tr"),
      // mr: getRotateControl("mr"),
      // br: getRotateControl("br"),
      // mb: getRotateControl("mb"),
      // bl: getRotateControl("bl"),
      // ml: getRotateControl("ml"),
      // tl: getRotateControl("tl"),
      // mt: getRotateControl("mt"),
    },
  });
})();

const createFabricObject = (options) => {
  return new fabric.CustomObject(options);
};

export default createFabricObject;
