import { fabric } from "fabric";

const { degreesToRadians } = fabric.util;

const getRotation = (position) => {
  switch (position) {
    case "mr":
      return 45;
    case "br":
      return 90;
    case "mb":
      return 135;
    case "bl":
      return 180;
    case "ml":
      return 225;
    case "tl":
      return 270;
    case "mt":
      return 315;
    case "tr":
    default:
      return 0;
  }
};

export function renderTriangleControl(
  ctx,
  left,
  top,
  styleOverride = {},
  fabricObject,
  position
) {
  const xSize =
    this.sizeX || styleOverride.cornerSize || fabricObject.cornerSize;
  const ySize =
    this.sizeY || styleOverride.cornerSize || fabricObject.cornerSize;

  const xSizeBy2 = xSize / 2;
  const ySizeBy2 = ySize / 2;

  const stroke =
    styleOverride.cornerStrokeColor || fabricObject.cornerStrokeColor;

  ctx.save();
  ctx.fillStyle = styleOverride.cornerColor || fabricObject.cornerColor;
  ctx.strokeStyle =
    styleOverride.cornerStrokeColor || fabricObject.cornerStrokeColor;

  ctx.translate(left, top);
  ctx.rotate(degreesToRadians(fabricObject.angle));

  ctx.rotate(degreesToRadians(getRotation(position)));

  ctx.beginPath();
  ctx.moveTo(-xSizeBy2, -ySizeBy2);
  ctx.lineTo(xSizeBy2, -ySizeBy2);
  ctx.lineTo(xSizeBy2, ySizeBy2);
  ctx.closePath();
  ctx.fill();
  if (stroke) {
    ctx.stroke();
  }
  ctx.restore();
}
