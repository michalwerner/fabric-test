import React, { memo } from "react";
import styled from "styled-components";

import useCanvas from "./../hooks/useCanvas";
import ColorControl from "./ColorControl";

const CanvasEl = styled.canvas`
  bottom: 0;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  z-index: 999;
`;

const CanvasComponent = memo(({ canvasRef }) => {
  return <CanvasEl ref={canvasRef} />;
});

const Canvas = () => {
  const [canvasRef, activeCustomObjectData] = useCanvas();

  return (
    <>
      <CanvasComponent canvasRef={canvasRef} />
      {activeCustomObjectData && (
        <ColorControl activeCustomObjectData={activeCustomObjectData} />
      )}
    </>
  );
};

export default Canvas;
