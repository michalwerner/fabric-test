import React from "react";
import styled from "styled-components";

import { randomColor } from "../utils/randomColor";

const ControlEl = styled.div.attrs((props) => ({
  style: { top: props.top, left: props.left },
}))`
  position: absolute;
  z-index: 999;
`;

const ColorControl = ({ activeCustomObjectData: { target, left, top } }) => {
  const handleClick = () => {
    target.set("fill", randomColor());
    target.canvas.renderAll();
  };

  return (
    <ControlEl top={top} left={left}>
      <button type="button" onClick={handleClick}>
        Change color
      </button>
    </ControlEl>
  );
};

export default ColorControl;
